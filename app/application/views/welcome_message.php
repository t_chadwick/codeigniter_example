<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>CodeIgniter Dev Page</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code, .code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	.container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body class="container">
<div>
	<h1>Welcome to CodeIgniter!</h1>

	<div>
		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

		<p>If you would like to edit this page you'll find it located at:</p>
		<code>application/views/welcome_message.php</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>application/controllers/Welcome.php</code>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="app/user_guide/">User Guide</a>.</p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>
<a href="/index.php/blog/"><button class="btn btn-primary btn-lg" type="button" name="button">Blog</button></a>
<header>
	<h1>CodeIgniter Example</h1>
	<p>This Page will go through the key steps of setting up a local php, CodeIgniter based, site.</p>
</header>
<section >
	<article class="">
		<h2>Steps</h2>
		<ol>
			<li>
				<h3>Create a New Local Site:</h3>
				<ol type="a">
					<li class="code">In /etc/apache2/sites-available -- Take a copy of the default config file and make one with your site's name.</li>
					<li class="code">In that file, replace the Server Name with the new site name. Also replace the Document Root with the location you are going to build your site(within /var/www/site-folder)</li>
					<li class="code">Go to /var/www and create the site folder referred to in the previous step.</li>
				</ol>
				<li>
					<h3>Start Building:</h3>
					<ol type="a">
						<li>Start Building Site however you want.</li>
					</ol>
				</li>
			</li>
		</ol>
		<p>Follow <a href="https://www.codeigniter.com/user_guide/installation/index.html">CodeIgniter Docs</a> for setup guide.</p>
		<h3>More Secure File System</h3>
		<p>For a More Secure setup, move the index.php file out of the CodeIgniter project directory.</p>
		<p>Then edit the following fields inside that file:</p>
		<ul>
			<li class="code">$system_path = '"project-folder"/system'</li>
			<li class="code">$application_folder = '"project-folder"/application'</li>
		</ul>
		<p>This will then mean that the root of the site will use the CodeIgniter application files.</p>
	</article>
</section>

</body>
</html>
