<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>CodeIgniter Dev Page</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code, .code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	.container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body class="container">
  <header>
    <h1>Blog</h1>
  </header>
  <p>This is the Blog Page</p>
  <a href="../"><button class="btn btn-primary btn-lg" type="button" name="button">Home</button></a>

  <?php
    if(isset($posts)):
      foreach($posts as $post):
        echo '<h1>' . $post . '</h1>';
        if(isset($content)):
          echo $content;
        else:
          echo "No Content!";
        endif;
      endforeach;
    else:
      echo "No Posts!";
    endif;
   ?>
</body>
</html>
